{-# LANGUAGE KindSignatures #-}

module Rehearsal.API where

import Control.Monad ((<=<))
import Control.Monad.Catch (MonadCatch(..), SomeException)
import Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Map as M
import Data.Proxy (Proxy(..))
import Data.Reflection (reifySymbol)
import GHC.TypeLits (Symbol)
import Servant (throwError)
import Servant.API ((:>), (:<|>)(..), Get, CaptureAll, Raw)
import Servant.HTML.Blaze (HTML)
import Servant.Server
       (Application, Handler, ServantErr(..), Server, err404, err500,
        serve)
import Servant.Utils.StaticFiles (serveDirectoryWebApp)
import System.FilePath ((<.>), joinPath)
import Text.Blaze.Html (Html)
import Text.Hamlet.Runtime
       (defaultHamletSettings, readHamletTemplateFile,
        renderHamletTemplate, toHamletData)

type RehearsalAPI (staticprefix :: Symbol) =
    "templates" :> CaptureAll "segments" String :> Get '[ HTML] Html :<|>
    staticprefix :> Raw

rehearsalAPI :: Proxy staticprefix -> Proxy (RehearsalAPI staticprefix)
rehearsalAPI _ = Proxy

hamletHandler :: FilePath -> Maybe String -> [String] -> Handler Html
hamletHandler root mLayout path =
    case mLayout of
        Just layout -> do
            innerTemplate <-
                toHamletData <$>
                (reportErrors $ renderTemplate M.empty templatePath)
            reportErrors $
                renderTemplate (M.singleton "widget" innerTemplate) $
                renderPath [layout]
        Nothing -> reportErrors $ renderTemplate M.empty templatePath
  where
    renderTemplate args =
        flip renderHamletTemplate args <=<
        readHamletTemplateFile defaultHamletSettings
    renderPath x = joinPath (root : x) <.> "hamlet"
    templatePath = renderPath path
    logError e = do
        liftIO $ putStrLn $ "Couldn't load: " ++ templatePath
        liftIO $ putStrLn $ "** Error: " ++ show e
    errorToErrBody mkErr e = mkErr {errBody = BSL.fromStrict . B8.pack $ show e}
    exceptionHandler mkErr e = do
        logError e
        throwError $ errorToErrBody mkErr e
    handleIOException (e :: IOError) = exceptionHandler err404 e
    handleOtherException (e :: SomeException) = exceptionHandler err500 e
    reportErrors f = f `catch` handleIOException `catch` handleOtherException

rehearsalServer ::
       FilePath -- | Template root
    -> FilePath -- | Static root
    -> Proxy staticprefix -- | Static prefix
    -> Maybe String
    -> Server (RehearsalAPI staticprefix)
rehearsalServer templateRoot staticRoot _ mLayout =
    hamletHandler templateRoot mLayout :<|> serveDirectoryWebApp staticRoot

startRehearsal ::
       FilePath -- | Template root
    -> FilePath -- | Static root
    -> String -- | Static prefix
    -> Maybe String -- | Optional layout
    -> Application
startRehearsal templateRoot staticRoot staticPrefix mLayout =
    reifySymbol staticPrefix $ \p ->
        serve
            (rehearsalAPI p)
            (rehearsalServer templateRoot staticRoot p mLayout)
