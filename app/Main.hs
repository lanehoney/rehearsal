module Main where

import Control.Monad.IO.Class (liftIO)
import Network.Wai.Handler.Warp (run)
import Options.Generic

import Rehearsal.API (startRehearsal)

data Options = Options
    { templateRoot :: FilePath
    , staticRoot :: FilePath
    , staticPrefix :: String
    , port :: Int
    , layout :: Maybe String
    } deriving (Show, Generic)

instance ParseRecord Options where
    parseRecord = parseRecordWithModifiers lispCaseModifiers

main :: IO ()
main = do
    Options tr sr sp p ml <-
        liftIO $ getRecord "Rehearsal: On-the-fly hamlet template compiler"
    run p (startRehearsal tr sr sp ml)
