# rehearsal
a utility for on-the-fly rendering of hamlet templates, useful for designers and
others who don't wish to run an entire application stack to develop out front
end templates.

*note*: at the moment, rehearsal doesn't support variable substitution and only
has limited support for layouts. it is intended purely for prototyping within
the hamlet ecosystem. it may eventually support configurable variable
substitution and greater widget support.

## install

1. [install stack](http://haskellstack.org/)
2. `stack build --copy-bins`

## usage

run `rehearsal-server --help` for runtime options. currently that includes:

```
Rehearsal: On-the-fly hamlet template compiler

Usage: rehearsal-server --template-root STRING --static-root STRING
                        --static-prefix STRING --port INT [--layout STRING]

Available options:
  -h,--help                Show this help text
```

### argument description

- `template-root`: the relative path to your templates directory. typically this
  will be `templates` in a yesod project.
- `static-root`: the relative path to your static assets (i.e. css, fonts, etc).
  typically this will be `static` in a yesod project.
- `static-prefix`: the route prefix your layout excpects static assets to appear
  in. this is commonly `assets` or `static`.
- `port`: the port the server should listen on.
- `layout`: the name of the template (relative path to `template-root` minus the
  file extension) to render as the layout. for now, this MUST insert a widget
  called `widget` as the child page.
  
### viewing templates

after running the server with the appropriate options, simply navigate to:

`http://localhost:<port>/templates/<templateName>` where `templateName` is the
path to the template you wish to render relative to the template root, without
the `.hamlet` extension. so for `template-root` of `./foo` a request for
template `http://localhost:<port>/templates/bar` will search for a template at
the path `./foo/bar.hamlet`, rendering a 404 if it can't be found. templates
that can't be compiled at runtime (i.e. require variables or expressions to be
evaluated) will return a 500 error explaining what went wrong.
  
